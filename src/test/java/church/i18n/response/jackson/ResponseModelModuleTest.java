/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.jackson;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import church.i18n.response.domain.ContextInfoDto;
import church.i18n.response.domain.Response;
import church.i18n.response.domain.ResponseContextInfo;
import church.i18n.response.domain.ResponseDto;
import church.i18n.response.domain.ResponseMessage;
import church.i18n.response.domain.ResponseMessageDto;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;
import com.fasterxml.jackson.dataformat.ion.IonFactory;
import com.fasterxml.jackson.dataformat.javaprop.JavaPropsFactory;
import com.fasterxml.jackson.dataformat.smile.SmileFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ResponseModelModuleTest {

  private static final ObjectMapper MAPPER = new ObjectMapper();
  private static final ContextInfoDto NULL_CONTEXT_INFO = new ContextInfoDto("name", null, null,
      null, null, null);
  private static final ResponseMessage NULL_MESSAGE = new ResponseMessageDto("code", null, null,
      null, null);
  private static final Response<String> NULL_RESPONSE = new ResponseDto<>(null, null, null);
  private static final Response<String> NULLABLE_RESPONSE = new ResponseDto<>("requestId",
      null, NULL_MESSAGE, List.of(NULL_MESSAGE,
      new ResponseMessageDto("code", null, null, null, List.of(NULL_CONTEXT_INFO))));
  private static final ContextInfoDto CTX = new ContextInfoDto("name", "type", "value", "format",
      "formatType", "message");
  private static final ResponseMessage MSG = new ResponseMessageDto("code", "message", "ERROR",
      "helpUri", null);
  private static final ResponseMessage MSG_CTX = new ResponseMessageDto("code", "message",
      "ERROR",
      "helpUri", List.of(CTX));
  private static final Response<String> RSP_MSG_CTX = new ResponseDto<>("UUID", "data",
      List.of(MSG_CTX));
  private static final Response<String> ERR_RSP_MSG_CTX = new ResponseDto<>("UUID", MSG,
      List.of(MSG_CTX));
  private static final Response<String> RSP = new ResponseDto<>("UUID", "data", null);
  private static final Response<String> RSP_MSG = new ResponseDto<>("UUID", "data",
      List.of(MSG));
  private static final ContextInfoDto CTX_incomplete = new ContextInfoDto("name", "type", "value",
      null, null, null);
  private static final ResponseMessage MSG_CTX_incomplete = new ResponseMessageDto("code",
      "message", "ERROR", null, List.of(CTX_incomplete));
  private static final Response<String> RSP_MSG_CTX_incomplete = new ResponseDto<>("UUID",
      null, List.of(MSG_CTX_incomplete));
  private static final Response<String> ERR_DATA_RSP_MSG_CTX = new ResponseDto<>("UUID",
      "data", MSG_CTX_incomplete, List.of(MSG_CTX_incomplete));
  private static final Response<String> ERR_RSP = new ResponseDto<>("UUID", MSG, null);
  private static final Response<String> ERR_RSP_MSG = new ResponseDto<>("UUID", MSG,
      List.of(MSG));
  private static final Response<String> ERR_DATA_RSP = new ResponseDto<>("UUID", "data",
      MSG, null);
  private static final Response<String> ERR_DATA_RSP_MSG = new ResponseDto<>("UUID", "data",
      MSG, List.of(MSG));
  private static JavaType STRING_RESPONSE_TYPE;

  @BeforeAll
  static void setup() {
    MAPPER.registerModule(new ResponseModelModule());
    STRING_RESPONSE_TYPE = MAPPER.getTypeFactory()
        .constructParametricType(ResponseDto.class, String.class);
  }

  @Test
  void contextInfo_noNullSerialized() throws IOException {
    final String value = MAPPER.writeValueAsString(NULL_CONTEXT_INFO);
    assertEquals("{\"name\":\"name\"}", value);
    assertEquals(NULL_CONTEXT_INFO, MAPPER.readValue(value, ContextInfoDto.class));
    assertContainsNoTypeInfo(value);
  }

  @Test
  void deserializeContextInfo_Ctx() throws IOException {
    final String value = MAPPER.writeValueAsString(CTX);
    assertEquals(CTX, MAPPER.readValue(value, ContextInfoDto.class));
    assertContainsNoTypeInfo(value);
  }

  @Test
  void deserializeResponseMessageMsg_Ctx() throws IOException {
    final String value = MAPPER.writeValueAsString(MSG_CTX);
    assertEquals(MSG_CTX, MAPPER.readValue(value, ResponseMessageDto.class));
    assertContainsNoTypeInfo(value);
  }

  @Test
  void deserializeResponseMessage_Msg() throws IOException {
    final String value = MAPPER.writeValueAsString(MSG);
    assertEquals(MSG, MAPPER.readValue(value, ResponseMessageDto.class));
    assertContainsNoTypeInfo(value);
  }

  @Test
  void deserializeRestResponse_Err_Data_Rsp() throws IOException {
    final String value = MAPPER.writeValueAsString(ERR_DATA_RSP);
    assertEquals(ERR_DATA_RSP, MAPPER.readValue(value, STRING_RESPONSE_TYPE));
    assertContainsNoTypeInfo(value);
  }

  @Test
  void deserializeRestResponse_Err_Data_Rsp_Msg() throws IOException {
    final String value = MAPPER.writeValueAsString(ERR_DATA_RSP_MSG);
    assertEquals(ERR_DATA_RSP_MSG, MAPPER.readValue(value, STRING_RESPONSE_TYPE));
    assertContainsNoTypeInfo(value);
  }

  @Test
  void deserializeRestResponse_Err_Data_Rsp_Msg_Ctx() throws IOException {
    final String value = MAPPER.writeValueAsString(ERR_DATA_RSP_MSG_CTX);
    assertEquals(ERR_DATA_RSP_MSG_CTX, MAPPER.readValue(value, STRING_RESPONSE_TYPE));
    assertContainsNoTypeInfo(value);
  }

  @Test
  void deserializeRestResponse_Err_Rsp() throws IOException {
    final String value = MAPPER.writeValueAsString(ERR_RSP);
    assertEquals(ERR_RSP, MAPPER.readValue(value, STRING_RESPONSE_TYPE));
    assertContainsNoTypeInfo(value);
  }

  @Test
  void deserializeRestResponse_Err_Rsp_Msg() throws IOException {
    final String value = MAPPER.writeValueAsString(ERR_RSP_MSG);
    assertEquals(ERR_RSP_MSG, MAPPER.readValue(value, STRING_RESPONSE_TYPE));
    assertContainsNoTypeInfo(value);
  }

  @Test
  void deserializeRestResponse_Err_Rsp_Msg_Ctx() throws IOException {
    final String value = MAPPER.writeValueAsString(ERR_RSP_MSG_CTX);
    assertEquals(ERR_RSP_MSG_CTX, MAPPER.readValue(value, STRING_RESPONSE_TYPE));
    assertContainsNoTypeInfo(value);
  }

  @Test
  void serializeResponseEmptyMessages() throws IOException {
    assertEquals("{\"requestId\":\"UUID\",\"data\":\"data\"}", MAPPER.writeValueAsString(RSP));
  }

  @Test
  void deserializeRestResponse_Rsp() throws IOException {
    final String value = MAPPER.writeValueAsString(RSP);
    assertEquals(RSP, MAPPER.readValue(value, STRING_RESPONSE_TYPE));
    assertContainsNoTypeInfo(value);
  }

  @Test
  void deserializeRestResponse_Rsp_Msg() throws IOException {
    final String value = MAPPER.writeValueAsString(RSP_MSG);
    assertEquals(RSP_MSG, MAPPER.readValue(value, STRING_RESPONSE_TYPE));
    assertContainsNoTypeInfo(value);
  }

  @Test
  void deserializeRestResponse_Rsp_Msg_Ctx() throws IOException {
    final String value = MAPPER.writeValueAsString(RSP_MSG_CTX);
    assertEquals(RSP_MSG_CTX, MAPPER.readValue(value, STRING_RESPONSE_TYPE));
    assertContainsNoTypeInfo(value);
  }

  @Test
  void deserializeRestResponse_Rsp_Msg_Ctx_incomplete() throws IOException {
    final String value = MAPPER.writeValueAsString(RSP_MSG_CTX_incomplete);
    assertEquals(RSP_MSG_CTX_incomplete, MAPPER.readValue(value, STRING_RESPONSE_TYPE));
    assertContainsNoTypeInfo(value);
  }

  @Test
  void responseMessage_noNullSerialized() throws IOException {
    final String value = MAPPER.writeValueAsString(NULL_MESSAGE);
    assertEquals("{\"code\":\"code\"}", value);
    assertEquals(NULL_MESSAGE, MAPPER.readValue(value, ResponseMessageDto.class));
    assertContainsNoTypeInfo(value);
  }

  @Test
  void restResponse_noNullSerialized() throws IOException {
    final String value = MAPPER.writeValueAsString(NULL_RESPONSE);
    assertEquals("{}", value);
    assertEquals(NULL_RESPONSE, MAPPER.readValue(value, STRING_RESPONSE_TYPE));
    assertContainsNoTypeInfo(value);

  }

  @Test
  void restResponse_noNullableContentSerialized() throws IOException {
    final String value = MAPPER.writeValueAsString(NULLABLE_RESPONSE);
    assertEquals("{\"requestId\":\"requestId\",\"error\":{\"code\":\"code\"},"
        + "\"messages\":[{\"code\":\"code\"},{\"code\":\"code\","
        + "\"context\":[{\"name\":\"name\"}]}]}", value);
    assertEquals(NULLABLE_RESPONSE, MAPPER.readValue(value, STRING_RESPONSE_TYPE));
    assertContainsNoTypeInfo(value);
  }

  @Test
  void test_YamlFormat() throws IOException {
    ObjectMapper yamlObjectMapper = new ObjectMapper(new YAMLFactory());
    yamlObjectMapper.registerModule(new ResponseModelModule());
    final String value = yamlObjectMapper.writeValueAsString(RSP_MSG_CTX);
    final String expectedString = """
        ---
        requestId: "UUID"
        data: "data"
        messages:
        - code: "code"
          message: "message"
          type: "ERROR"
          helpUri: "helpUri"
          context:
          - name: "name"
            value: "type"
            valueType: "value"
            help: "format"
            helpType: "formatType"
            message: "message"
        """;
    assertEquals(expectedString, value);
    final Object actual = yamlObjectMapper.readValue(expectedString, STRING_RESPONSE_TYPE);
    assertEquals(RSP_MSG_CTX, actual);
  }

  @Test
  void test_PropertiesFormat() throws IOException {
    ObjectMapper propertiesObjectMapper = new ObjectMapper(new JavaPropsFactory());
    propertiesObjectMapper.registerModule(new ResponseModelModule());
    final String value = propertiesObjectMapper.writeValueAsString(RSP_MSG_CTX);
    assertEquals("""
        requestId=UUID
        data=data
        messages.1.code=code
        messages.1.message=message
        messages.1.type=ERROR
        messages.1.helpUri=helpUri
        messages.1.context.1.name=name
        messages.1.context.1.value=type
        messages.1.context.1.valueType=value
        messages.1.context.1.help=format
        messages.1.context.1.helpType=formatType
        messages.1.context.1.message=message
        """, value);
  }

  @Test
  void test_CborFormat() throws IOException {
    ObjectMapper cborObjectMapper = new ObjectMapper(new CBORFactory());
    cborObjectMapper.registerModule(new ResponseModelModule());
    final byte[] bytes = cborObjectMapper.writeValueAsBytes(RSP_MSG_CTX);
    final Object actual = cborObjectMapper.readValue(bytes, STRING_RESPONSE_TYPE);
    assertEquals(RSP_MSG_CTX, actual);
  }

  @Test
  void test_SmileFormat() throws IOException {
    ObjectMapper smileObjectMapper = new ObjectMapper(new SmileFactory());
    smileObjectMapper.registerModule(new ResponseModelModule());
    final byte[] bytes = smileObjectMapper.writeValueAsBytes(RSP_MSG_CTX);
    final Object actual = smileObjectMapper.readValue(bytes, STRING_RESPONSE_TYPE);
    assertEquals(RSP_MSG_CTX, actual);
  }

  @Test
  void test_IonFormat() throws IOException {
    ObjectMapper ionObjectMapper = new ObjectMapper(new IonFactory());
    ionObjectMapper.registerModule(new ResponseModelModule());
    final byte[] bytes = ionObjectMapper.writeValueAsBytes(RSP_MSG_CTX);
    final Object actual = ionObjectMapper.readValue(bytes, STRING_RESPONSE_TYPE);
    assertEquals(RSP_MSG_CTX, actual);
  }

  private void assertContainsNoTypeInfo(final String value) {
    assertFalse(value.contains(ContextInfoDto.class.getSimpleName()));
    assertFalse(value.contains(ResponseContextInfo.class.getSimpleName()));
    assertFalse(value.contains(ResponseMessage.class.getSimpleName()));
    assertFalse(value.contains(ResponseMessageDto.class.getSimpleName()));
    assertFalse(value.contains(Response.class.getSimpleName()));
    assertFalse(value.contains(ResponseDto.class.getSimpleName()));
    assertFalse(value.contains("@"));
  }
}
