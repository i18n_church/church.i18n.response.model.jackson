module church.i18n.response.model.jackson {
  requires static org.jetbrains.annotations;

  requires transitive com.fasterxml.jackson.core;
  requires transitive com.fasterxml.jackson.databind;

  requires church.i18n.response;

  exports church.i18n.response.jackson;
}