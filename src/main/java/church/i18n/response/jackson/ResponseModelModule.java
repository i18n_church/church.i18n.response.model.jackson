/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.response.jackson;

import church.i18n.response.domain.ContextInfoDto;
import church.i18n.response.domain.ResponseContextInfo;
import church.i18n.response.domain.ResponseDto;
import church.i18n.response.domain.ResponseMessage;
import church.i18n.response.domain.ResponseMessageDto;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.jetbrains.annotations.NotNull;

public class ResponseModelModule extends Module {

  @Override
  public final @NotNull String getModuleName() {
    return ResponseModelModule.class.getSimpleName();
  }

  @Override
  public final @NotNull Version version() {
    return VersionUtil.parseVersion("0.1.0", "church.i18n",
        "church.i18n.response.model.jackson");
  }

  @Override
  public final void setupModule(final @NotNull SetupContext context) {
    SimpleModule module = new SimpleModule();

    module.addAbstractTypeMapping(ResponseContextInfo.class, ContextInfoDto.class);
    module.addAbstractTypeMapping(ResponseMessage.class, ResponseMessageDto.class);
    module.setMixInAnnotation(ResponseDto.class, ResponseDtoMixin.class);
    module.setMixInAnnotation(ResponseMessageDto.class, ResponseMessageDtoMixin.class);
    module.setMixInAnnotation(ContextInfoDto.class, ContextInfoDtoMixin.class);

    module.setupModule(context);
  }
}
