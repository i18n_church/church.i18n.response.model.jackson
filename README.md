A Jackson module for a model serialization and deserialization of
`church.i18n.response.model` classes by a
[FasterXML Jackson](https://mvnrepository.com/artifact/com.fasterxml.jackson.core)
serialization engine.

The implementation supports the following formats for serialization and deserialization:

- json
- yaml
- properties
- cbor
- smile
- ion

**XML** is not supported yet,
[due to an issue](https://github.com/FasterXML/jackson-dataformat-xml/issues/187). Once time
permits, it could be resolved.

### Spring boot configuration example

```java
@Configuration
public class AppConfig extends WebMvcConfigurationSupport {

  @Bean
  public Jackson2ObjectMapperBuilder jacksonBuilder() {
    return new Jackson2ObjectMapperBuilder()
        .modules(new ResponseModelModule());
  }
}
```
